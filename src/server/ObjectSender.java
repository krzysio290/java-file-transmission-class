package server;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ObjectSender implements Runnable {
	ObjectOutputStream ous = null;
	File file = null;
	String fileName;
	String host;
	int port;

	public ObjectSender(String fileName, String hostname, int port) {
		this.file = new File(fileName);
		this.host = hostname;
		this.port = port;
	}

	public ObjectSender(ObjectOutputStream ous, String fileName, String hostname, int port) {
		this.file = new File(fileName);
		this.fileName = fileName;
//		this.host = hostname;
//		this.port = port;
		this.ous = ous;
	}

	@Override
	public void run() {
		System.out.println("ObjectSender Start");
		// System.out.println(myFile.length());
		try {
			byte[] myFileContent = new byte[(int) file.length()];
			DataInputStream dis = new DataInputStream(new FileInputStream(file));
			dis.read(myFileContent, 0, myFileContent.length);
			dis.close();

			// SocketChannel sChannel = SocketChannel.open();
			// sChannel.configureBlocking(true);
			// if (sChannel.connect(new InetSocketAddress(host, port))) {
			// //Wyslanie do serwera
			// ObjectOutputStream oos = new
			// ObjectOutputStream(sChannel.socket().getOutputStream());
			ObjectOutputStream oos = ous;
			oos.writeObject(fileName);
			Thread.sleep(200);
			oos.writeObject(myFileContent);
			oos.flush();
			//oos.close();
			// }
		} catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("InterruptedException: " + e.getMessage());
		}
		System.out.println("ObjectSender End");
	}
}
