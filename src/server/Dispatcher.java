package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Dispatcher implements Runnable {
	protected ObjectInputStream  ois = null;
	protected ObjectOutputStream ous = null;
	protected String host = "localhost";
	protected int port = 12345;
	
	public Dispatcher(ObjectInputStream  inputObjectStream, ObjectOutputStream outputObjectStream) {
		ois = inputObjectStream;
		ous = outputObjectStream;
	}
	
	@Override
	public void run() {
		System.out.println("Dispatcher start");
		String filename = null;
		ObjectReceiver objectReceiver = new ObjectReceiver(ois, filename);
		Thread receiverThread = new Thread(objectReceiver);
		Thread senderThread = null;
		receiverThread.start();
		try {
			receiverThread.join();
			System.out.println("Wypelniwszy: " + objectReceiver.getFilename());
			String name = objectReceiver.getFilename();
			String trimmed = name.substring(name.lastIndexOf("/")+1, name.length());
			System.out.println(trimmed);
			//File name to retrieve: '/storage/sdcard0/Music/01cd.mp3'
			if(objectReceiver.properties.elementAt(1).equals("amplify")) {
				System.out.println("Amplifying function - start");
				String result = TerminalExecutor.executeCommandSync("./amplify.sh " + trimmed + " " + objectReceiver.properties.elementAt(2));
				System.out.println(result);
				System.out.println("Amplifying function - end");
			} else {
				System.out.println("Another option");
			}
			//System.out.println(TerminalExecutor.executeCommandSync("ls"));
			senderThread = new Thread(new ObjectSender(ous, trimmed, host, port));
			senderThread.start();
			senderThread.join();
			ous.close();
			ois.close();
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
		System.out.println("Dispatcher end");
	}

}
