package server;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Vector;

public class ObjectReceiver implements Runnable {

	protected ObjectInputStream ois = null;
	protected DataOutputStream bos = null;
	protected String host;
	protected int port;
	protected String name;
	protected byte[] fileContent = null;
	public Vector<String> properties;
	
	public String getFilename() {
		return name;
	}
	public ObjectReceiver(ObjectInputStream inputStream, String filename) {
		ois = inputStream;
		this.name = filename;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			properties = (Vector<String>) ois.readObject();
			
			System.out.println("File name to retrieve: '" + properties.elementAt(0) + "'");
			this.name = new String(properties.elementAt(0)); //copy of filename
			String trimmed = name.substring(name.lastIndexOf("/")+1, name.length());
			File file = new File(trimmed);
			fileContent = (byte[]) ois.readObject();
			bos = new DataOutputStream(new FileOutputStream(file));
			System.out.println("Content size: " + fileContent.length);
			bos.write(fileContent);
			bos.close();
			System.out.println("Connection ended");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}
}
