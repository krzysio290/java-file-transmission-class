package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class Server {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Server Start");
		try {
			ServerSocketChannel ssChannel = ServerSocketChannel.open();
			ssChannel.configureBlocking(true);
			int port = 12345;
			ssChannel.socket().bind(new InetSocketAddress(port));
			
			while (true) {
				System.out.println("Waiting for new connection");
				SocketChannel sChannel = ssChannel.accept();
				
				ObjectInputStream  ois = new ObjectInputStream(sChannel.socket().getInputStream());
				ObjectOutputStream ous = new ObjectOutputStream(sChannel.socket().getOutputStream());
				//odcinka do nowego wątku
//				Thread receiverThread = new Thread(new ObjectReceiver(ois));
//				receiverThread.start();
				Thread dispatcher = new Thread(new Dispatcher(ois, ous));
				dispatcher.start();
			}
		} catch (IOException exception) {
			System.out.println(exception.getMessage());
		}
		System.out.println("Server closing...");
	}

}
