package server;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

public class TerminalExecutor {
	TerminalExecutor() {

	}

	static String executeCommandSync(String cmd) throws InterruptedException, IOException {
		return exec(cmd,false);
	}

	static void executeCommandAsync(String cmd) throws InterruptedException, IOException {
		exec(cmd,true);
	}

	static private String exec(String cmd, boolean silent) throws InterruptedException, IOException {
		final Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(cmd);
		if (!silent) {
			proc.waitFor();
		}
		InputStream is = proc.getInputStream();
		StringWriter writer = new StringWriter();
		IOUtils.copy(is, writer, Charset.defaultCharset());
		String out = writer.toString();
//		System.out.println(out);
		return out;
	}

}
